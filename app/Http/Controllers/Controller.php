<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getCity(Request $request){
        try{
            $url = 'http://api.openweathermap.org/data/2.5/weather?q=';
            $url .= $request['city'];
            $url .= '&units=metric&appid=52dc04ccfff95287145b04c79b35edd5';

            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json'
            ]);
            $response = curl_exec($curl);
            curl_close($curl);
            $response = json_decode($response);

            if(isset($response->cod) && $response->cod != 404){
                $url2 = 'http://api.openweathermap.org/data/2.5/onecall?lat=';
                $url2 .= $response->coord->lat.'&lon=';
                $url2 .= $response->coord->lon;
                $url2 .= '&exclude=minutely,hourly&units=metric&appid=52dc04ccfff95287145b04c79b35edd5';
                $curl2 = curl_init($url2);
                curl_setopt($curl2, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl2, CURLOPT_HTTPHEADER, [
                    'Content-Type: application/json'
                ]);
                $response2 = curl_exec($curl2);
                curl_close($curl2);
                $response2 = json_decode($response2);
                $dailyWeather = [];
                foreach($response2->daily as $key => $day){

                    if($key == 0){
                        $dailyWeather[] = [
                            'day' => date('d/m/Y', $day->dt),
                            'pressure' => $day->pressure.'hPa',
                            'humidity' => $day->humidity.'%',
                            'wind' => $day->wind_speed.' m/s',
                            'min_max' => number_format($day->temp->max).'º / '.number_format($day->temp->min).'º',
                            'current_temp' => number_format($response2->current->temp)
                        ];
                    } else {
                        $dailyWeather[] = [
                            'day' => date('d/m/Y', $day->dt),
                            'pressure' => $day->pressure.'hPa',
                            'humidity' => $day->humidity.'%',
                            'wind' => $day->wind_speed.' m/s',
                            'min_max' => number_format($day->temp->max).'º / '.number_format($day->temp->min).'º',
                            'current_temp' => number_format($day->temp->max)
                        ];
                    }


                }

                $results = [
                    'city' => $response->name.', '.$response->sys->country,
                    'coord' => '['.$response->coord->lat.','.$response->coord->lon.']',
                    'weather' => $dailyWeather,
                    'success' => true,
                ];
            } else {
                $results = [
                    'success' => false,
                ];
            }

            return response()->json([
                'results' => $results,
            ]);
        }catch (\Exception | \Throwable $e) {
            return response()->json($e->getMessage(), 500);
        }

    }
}
