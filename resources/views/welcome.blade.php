<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Pedro Henriques Test</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;600&display=swap" rel="stylesheet">

        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js" integrity="sha512-bZS47S7sPOxkjU/4Bt0zrhEtWx0y0CRkhEp8IckzK+ltifIIE9EMIMTuT/mEzoIMewUINruDBIR/jJnbguonqQ==" crossorigin="anonymous"></script>

        <!-- Jquery Min -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

        <!-- CSS -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="container">
            <div class="col-12">
                <div class="row">
                    <div class="col-12 col-lg-6">
                        <h1 class="text-uppercase">
                            Weather forecast for the next 7 days
                        </h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-lg-4">
                        <h2 class="text-capitalize">
                            Lorem Ipsum
                        </h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-lg-6 col-xl-4">
                        <div class="card h-100">
                            <div class="card-body p-0">
                                <h3 class="card-title">Lorem Ipsum</h3>
                                <p class="card-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                            </div>
                            <div class="card-footer p-0">
                                <button>see more <i class="fas fa-long-arrow-alt-right pl-1"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 col-xl-4">
                        <div class="card h-100">
                            <div class="card-body p-0">
                                <h3 class="card-title">Lorem Ipsum</h3>
                                <p class="card-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                            </div>
                            <div class="card-footer p-0">
                                <button>see more <i class="fas fa-long-arrow-alt-right pl-1"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 col-xl-4">
                        <div class="card h-100">
                            <div class="card-body p-0">
                                <h3 class="card-title">Lorem Ipsum</h3>
                                <p class="card-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                            </div>
                            <div class="card-footer p-0">
                                <button>see more <i class="fas fa-long-arrow-alt-right pl-1"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-lg-10 offset-lg-1">
                        <div class="row">
                            <div class="col-12">
                                <input id="myInput" type="text" placeholder="Search for a specific city...">
                                <span class="fas fa-search search-icon"></span><span class="bar">|</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mx-auto input-dropdown d-none">
                                <div class="row city">
                                    <div class="col-12">
                                        <p class="dropdown-title"></p>
                                    </div>
                                </div>
                                <div class="row today-weather mx-auto today">

                                </div>
                                <div class="row other-weather mx-auto other-day">

                                </div>
                            </div>
                            <div class="mx-auto no-result d-none">
                                <div class="row city">
                                    <div class="col-12">
                                        <p class="no-result-title">No results found</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script>
            var AJAXURL = 'http://baselaravel.test';

            $( document ).ready(function() {
                var globalTimeout = null;
                $('#myInput').keyup(function() {
                    if (globalTimeout != null) {
                        clearTimeout(globalTimeout);
                    }
                    globalTimeout = setTimeout(function() {
                        globalTimeout = null;
                        filterFunction()
                    }, 200);
                });

                requestWeather('Lisbon');
            });

            function  filterFunction() {
                $('.dropdown-title').html('');
                $('.today-weather').html('');
                $('.other-weather').html('');
                $('.input-dropdown').addClass('d-none');
                $('.no-result').addClass('d-none');
                setTimeout(function(){ requestWeather($('#myInput').val()); }, 1000);
            }

            function requestWeather(city){
                $.ajax({
                    cache: false,
                    type: 'POST',
                    url: '{{ action('\App\Http\Controllers\Controller@getCity') }}',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "city": city,
                    },
                    success: function (response) {
                        let result = response.results
                        if(result.success){
                            $('.dropdown-title').html(result.city+'<span class="coordinates"><i class="fas fa-map-pin"></i>'+result.coord+'</span>');
                            let weather = result.weather;
                            let otherWeather = '';
                            let weatherIcon = '';
                            for(let i=0;i<weather.length;i++){
                                if(weather[i].current_temp < 0){
                                    weatherIcon = '<i class="fas fa-icicles"></i>';
                                }else if(weather[i].current_temp >= 0 && weather[i].current_temp < 11){
                                    weatherIcon = '<i class="fas fa-snowflake"></i>';
                                }else if(weather[i].current_temp >= 11 && weather[i].current_temp < 25){
                                    weatherIcon = '<i class="far fa-sun"></i>';
                                }else{
                                    weatherIcon = '<i class="fas fa-fire"></i>';
                                }
                                if(i===0){
                                    $('.today-weather').html('<div class="col-12 col-lg-6 col-xl-3"><div class="row"><div class="col-12"><p class="today-title text-capitalize">Today</p></div></div><div class="row"><div class="col-12"><p class="today-date pt-0">'+ weather[i].day +'</p></div></div></div><div class="col-12 col-lg-6"><p class="temperature text-center">'+ weatherIcon + weather[i].current_temp +'</p></div><div class="col-12 col-xl-3"><div class="row"><div class="col-12 col-sm-6 col-xl-12"><p class="whether my-0"><i class="fas fa-temperature-low"></i>'+ weather[i].min_max +'</p></div><div class="col-12 col-sm-6 col-xl-12"><p class="whether my-0"><i class="fas fa-wind"></i>'+ weather[i].wind +'</p></div><div class="col-12 col-sm-6 col-xl-12"><p class="whether my-0"><i class="far fa-humidity"></i>'+ weather[i].humidity +'</p></div><div class="col-12 col-sm-6 col-xl-12"><p class="whether my-0 pb-25"><i class="fas fa-long-arrow-alt-down"></i>'+ weather[i].pressure +'</p></div></div></div>');
                                }else{
                                    otherWeather += '<div class="col-12 col-lg-6 col-xl-3 pt-5 pt-lg-0"><div class="row"><div class="col-12"><p class="other-day-date pt-0">'+ weather[i].day +'</p></div></div></div><div class="col-12 col-lg-6"><p class="temperature text-center">'+ weatherIcon + weather[i].current_temp +'</p></div><div class="col-12 col-xl-3"><div class="row"><div class="col-12 col-sm-6 col-xl-12"><p class="whether my-0"><i class="fas fa-temperature-low"></i>'+ weather[i].min_max +'</p></div><div class="col-12 col-sm-6 col-xl-12"><p class="whether my-0"><i class="fas fa-wind"></i>'+ weather[i].wind +'</p></div><div class="col-12 col-sm-6 col-xl-12"><p class="whether my-0"><i class="far fa-humidity"></i>'+ weather[i].humidity +'</p></div><div class="col-12 col-sm-6 col-xl-12"><p class="whether my-0 pb-25"><i class="fas fa-long-arrow-alt-down"></i>'+ weather[i].pressure +'</p></div></div></div>';
                                }
                            }
                            $('.other-weather').html(otherWeather);
                            $('.input-dropdown').removeClass('d-none');
                        } else {
                            $('.input-dropdown').addClass('d-none');
                            $('.no-result').removeClass('d-none');
                        }

                    },
                    error: function (error) {
                        $('.input-dropdown').addClass('d-none');
                        $('.no-result').removeClass('d-none');
                        console.log("error = ", error);
                    }
                });
            }
        </script>
    </body>
</html>
